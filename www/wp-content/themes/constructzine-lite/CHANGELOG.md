

### 1.2.2 - 03/12/2014

 Changes: 


 * Update style.css


### 1.2.1 - 03/12/2014

 Changes: 


 * fixed comments reply and readme typo
 * Fixed customizer sanitization and tgm plugin error
 * #2 - small font on single page

Added in style.css, a font-size for all the paragraphs in post entry
class
 * This fixes #2

Added style for all the paragraphs with the 'post-entry' class, for text
esthethics
 * This fixes #2 - new

Removed !important, added the proper attributes in the correct class
 * This fixes #3

Added valid translation-ready in all .php files
- function -> fload_theme_textdomain
- other .php files -> 'constructzine-lite' argument

Added a few tags to the template (custom-header, yellow, gray, black)
 * This fixes #4

Added a credit link bellow the map widget. The links have the 'nofollow'
property similar with other template copyright standards.
 * Merge pull request #5 from DragosBubu/development

Development
 * Fixed some translations domains, css style for single page and footer link, fixed footer link, added more tags
 * Update style.css


### 1.1 - 17/10/2014

 Changes: 


 * First version
 * improved images and changed encoding
 * Fixed wordpress.org 20326 ticket
 * Remove archive from theme folder
