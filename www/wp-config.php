<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи и ABSPATH. Дополнительную информацию можно найти на странице
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется скриптом для создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения вручную.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'freshlemon-dostavka');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';xr/#3O_!A|#$C}/tBX:zWz@548)}sVmev|gTn6&M@yfy_VXUV2/7@- ZM5sldC|');
define('SECURE_AUTH_KEY',  'lHS{vCAc}p1INU W>0TJ<$XaFm{% L=SDgVpArJ^>g3bsAHMw{[C {zrk2D|EIC%');
define('LOGGED_IN_KEY',    '0R;>P6XQrUBNX*@+S9 :_rip =R[cf ~xA<aT!+i$3js;-|h3jZe-W8|%Me@dq%W');
define('NONCE_KEY',        'SY=BS2Lj}/|&Xfw3[[*T9qDb_ Jh`(Gwl:aCyXgnk:mwR)QZ0K$Fvn=Gz+kwKc46');
define('AUTH_SALT',        'Y],D-/V0g!>xC!+*z -+m7kIE+L$gAV$H{[NGT</Umlvt_Is#PoH-33j]Ljxcpbz');
define('SECURE_AUTH_SALT', 'e9FYnT)x3lP[<|nWY8AROQfYt]elF5$H(Vrj-2zQ:z>s_?`C|/:[-J|R>.U`==a[');
define('LOGGED_IN_SALT',   'w;g|2w[l)z.)QS_7Tsa)|~|wVq6n:Ak&7u:m#gf*`@80bC!5aCqA vlge>0&XIqs');
define('NONCE_SALT',       ':%C3z1m|k]s}dL^kw4Dg.##q![t[!T126u!3UcEAi8>P<t$yg=^OmM6-{[y2O_;)');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'dst_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
